package com.webdroidsolutions.fynrex;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wang.avi.AVLoadingIndicatorView;

import im.delight.android.webview.AdvancedWebView;

public class MainActivity extends AppCompatActivity implements AdvancedWebView.Listener {

    private AdvancedWebView mWebView;
    LinearLayout noInternetConnectionBox;
    ImageView noInternetConnectionImage;
    LinearLayout splashBox;
    Animation logoAnimation, lineAnimation, textAnimation;
    ImageView splashLogo, splashLine, splashText;
    //AVLoadingIndicatorView avLoadingIndicatorView;
    private String LastUrl = "";

    String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        noInternetConnectionBox = (LinearLayout) findViewById(R.id.no_internet_connection_box);
        noInternetConnectionImage = (ImageView) findViewById(R.id.no_internet_connection_image);

        noInternetConnectionBox.setVisibility(View.GONE);
        noInternetConnectionImage.setVisibility(View.GONE);

        mWebView = (AdvancedWebView) findViewById(R.id.webview);
        //mWebView.setVisibility(View.GONE);

        splashBox = (LinearLayout) findViewById(R.id.splash_box);

//        avLoadingIndicatorView=(AVLoadingIndicatorView)findViewById(R.id.avlLoadingIndicator);
//        avLoadingIndicatorView.setVisibility(View.VISIBLE);
        mWebView.setListener(this, this);
        //url=getIntent().getStringExtra("url");
        url = "http://webdroidsolutions.com/fynrex/public/";
        mWebView.loadUrl(url);

        splashLogo = (ImageView) findViewById(R.id.splash_logo);
        splashLine = (ImageView) findViewById(R.id.splash_line);
        splashText = (ImageView) findViewById(R.id.splash_text);
        logoAnimation = AnimationUtils.loadAnimation(this, R.anim.zoom_in1);
        lineAnimation = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
        textAnimation = AnimationUtils.loadAnimation(this, R.anim.zoom_in2);

        splashLogo.startAnimation(logoAnimation);
        lineAnimation.setStartOffset(1000);
        splashLine.startAnimation(lineAnimation);
        textAnimation.setStartOffset(1600);
        splashText.startAnimation(textAnimation);

        logoAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        lineAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        textAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        //mWebView.setVisibility(View.VISIBLE);
                        splashBox.setVisibility(View.GONE);
                    }
                }, 1500);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void refreshUrl(View view) {
        mWebView = (AdvancedWebView) findViewById(R.id.webview);
//        avLoadingIndicatorView=(AVLoadingIndicatorView)findViewById(R.id.avlLoadingIndicator);
//        avLoadingIndicatorView.setVisibility(View.VISIBLE);
        mWebView.setListener(this, this);
        mWebView.loadUrl(LastUrl);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onBackPressed() {
        if (!mWebView.onBackPressed()) { return; }
        // ...
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        Log.e("page" , "started");
        //avLoadingIndicatorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageFinished(String url) {
        Log.e("page" , "finish: "+url);
        mWebView.setVisibility(View.VISIBLE);
        //splashBox.setVisibility(View.GONE);
        noInternetConnectionBox.setVisibility(View.GONE);
        noInternetConnectionImage.setVisibility(View.GONE);
        //avLoadingIndicatorView.setVisibility(View.GONE);
    }

    @Override
    public void onPageError(int errorCode, String description, final String failingUrl) {
        Log.e("page" , errorCode+"");
        Log.e("page" , description);
        mWebView.setVisibility(View.GONE);
        //avLoadingIndicatorView.setVisibility(View.GONE);
        AlertDialog alertDialog=new AlertDialog.Builder(MainActivity.this)
                .setTitle("No Internet")
                .setMessage("Connect to internet and try again")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        noInternetConnectionBox.setVisibility(View.VISIBLE);
                        noInternetConnectionImage.setVisibility(View.VISIBLE);
                        LastUrl = failingUrl;
                        //finish();
                    }
                }).create();
        alertDialog.show();
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }


}
